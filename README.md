Homework assignment no. 2, Cache Implementation
====================================

**Publication date:** 10th April 

**Submission deadline:** 3th May 23:59

General information
-------------------
The goal of this homework is to implement caches with various eviction policies.

For our purposes cache is a software component capable of storing data, with limited capacity, fast access time and defined behaviour when element is inserted on full capacity.
In reality the accessing items in cache should have constant O(1) time complexity. 
For the purpose of this assignment we encourage you to try your best. The purpose of this assignment is to understand things, not to optimize eviction algorithms. 

### Eviction Policies
Eviction policy defines which element is removed when new element is inserted and the cache is full.
In this homework caches with the following eviction policies will be implemented

* **Random** -- a random entry is removed 
* **Least Recently Used (LRU)** -- the least used entry is removed
* **Most Recently Used (MRU)** -- the most used entry is removed
* **First In First Out (FIFO)** -- the oldest entry is removed

### Generic Classes
Java Generic methods and generic classes enable programmers to specify, with a single method declaration, a set of related methods, or with a single class declaration, a set of related types, respectively.

For the purpose of this assignment you can understand generics as "companion types". For example take this ```Box``` class

```java
class Box {
    private String content;
    
    public void put(String content) { this.content = content; }
    
    public String get() { return content; }
}
```
This isn't a very good box, is it? No matter how many instances you created, you can only put Strings in it. 
Perhaps we could create many different variants of this class (e.g. ``IntBox``, ``StringBox``...), however we could never generate all possible data types.
What we can do is to add a generic type to our ```Box``` class, which will declare the type of its content.

```java
class Box<T> {
    private T content;
    
    public void put(T content) { this.content = content; }
    
    public T get() { return content; }
}
```

Now I can create many boxes for different types in it. 

```java
Box<Integer> intBox = new Box<>();
intBox.put(1);

Box<String> stringBox = new Box<>():
stringBox.put("Ahoj");
```

More detailed description can be found [in this tutorial](https://www.geeksforgeeks.org/generics-in-java/) or simply google for "Java Generics". 
Things such as wildcard types or bout type parameters are not required for this assignment. Neither is the understanding of generic methods.


### Evaluation
Beside functional correctness this assignment is focused on clean implementation and your understanding of Java's collection framework.
 
The maximum number of points for this assignment is **9**.

- **5.5 points** for passing tests (attached tests do not guarantee a 100% correctness).
- **3.5 points** for correct, efficient and clean implementation (evaluated by your class teacher).

Note that all this is at your seminar teacher's discretion.

### Preconditions
To successfully implement this assignment you need to know the following

1. Working with Interfaces
2. Understanding of method overloading and class inheritance 
3. Working with collections
4. Understanding of the above mentioned eviction types
5. Have a [basic understanding of generics in Java](https://www.geeksforgeeks.org/generics-in-java/)

### Project structure
The structure of project provided as a base for your implementation should meet the following criteria.

1. Package ```cz.muni.fi.pb162.hw02``` contains classes and interfaces provided as part of the assignment.
  - **Do not modify or add any classes or subpackages into this package.**
2. Package  ```cz.muni.fi.pb162.hw02.impl``` should contain your implementation.
  - **Anything outside this package will be ignored during evaluation.**

### Names in this document
Unless fully classified name is provided, all class names are relative to  package ```cz.muni.fi.pb162.hw02``` or ```cz.muni.fi.pb162.hw02.impl``` for classes implemented as part of your solution.

### Compiling the project
The project can be compiled and packaged in the same way you already know 

```bash
$ mvn clean compile
```

The only difference is, that unlike with seminar project, this time checks for missing documentation and style violation will produce an error. 
You can temporarily disable this behavior when running this command. 

```bash
$ mvn clean compile -Dcheckstyle.fail=false
```

You can consult your seminar teacher to help you set the ```checkstyle.fail``` property in your IDE (or just google it). 

### Submitting the assignment
The procedure to submit your solution may differ based on your seminar group. However generally it should be ok to submit ```target/homework01-2020-1.0-SNAPSHOT-sources.jar``` to the homework vault.

## Implementation

### Step 1. Cache Implementation
In this assignment you are expected to create a cache for each of the previously listed eviction policies. For example:

* ```cz.muni.fi.pb162.hw02.impl.LRUCache```
* ```cz.muni.fi.pb162.hw02.impl.MRUCache```
* ```cz.muni.fi.pb162.hw02.impl.FIFOCache```
* ```cz.muni.fi.pb162.hw02.impl.RandomCache```

All cache classes should implement the ```cz.muni.fi.pb162.hw02.Cache``` interface

### Step 2. Cache Factory
In the second step you should create a class named ```DefaultCacheFactory``` implementing the  ```cz.muni.fi.pb162.hw02.CacheFactory``` interface. 
This class will be responsible for producing instances of various factory (in programming this is a pattern know as -- surprise --  *factory*).
A factory class hides the instantiation logic. In other words it makes our API somewhat nicer as no matter what the constructor of each cache is, they can all be created the same way by using this factory.  