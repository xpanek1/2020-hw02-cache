package cz.muni.fi.pb162.hw02.impl;

import cz.muni.fi.pb162.hw02.Cache;
import cz.muni.fi.pb162.hw02.CacheFactory;
import cz.muni.fi.pb162.hw02.CacheType;
import org.junit.Before;
import org.junit.Test;

import java.util.ArrayList;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;

/**
 *
 * @author Jakub Cechacek
 */
public class RandomCacheTest {

    private static CacheFactory<Integer, Integer> factory = new DefaultCacheFactory<>();
    private Cache<Integer, Integer> cache;

    @Before
    public void setup() {
        this.cache = factory.create(CacheType.RANDOM, 2);
    }

    @Test
    public void cacheShouldStartEmpty() {
        assertThat(cache.get(1)).isNull();
    }

    @Test
    public void entriesShouldBeInsertedWithoutEvictionUnderCapacity() {
        cache.put(1, 1);
        assertThat(cache.get(1)).isEqualTo(1);
        assertThat(cache.get(2)).isNull();
        cache.put(2, 4);
        assertThat(cache.get(1)).isEqualTo(1);
        assertThat(cache.get(2)).isEqualTo(4);
    }

    @Test
    public void randomEntryShouldBeEvictedOnCapacityReach() {
        cache.put(1, 1);
        cache.put(2, 4);
        cache.put(3, 9);

        List<Integer> oldEntries = new ArrayList<>();
        oldEntries.add(cache.get(1));
        oldEntries.add(cache.get(2));

        assertThat(oldEntries).containsOnlyOnce((Integer) null);
        assertThat(cache.get(3)).isEqualTo(9);
    }
}
