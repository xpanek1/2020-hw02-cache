package cz.muni.fi.pb162.hw02.impl;

import cz.muni.fi.pb162.hw02.Cache;
import cz.muni.fi.pb162.hw02.CacheFactory;
import cz.muni.fi.pb162.hw02.CacheType;
import org.assertj.core.api.SoftAssertions;
import org.junit.Before;
import org.junit.Test;

/**
 *
 * @author Jakub Cechacek
 */
public class CacheFactoryTest {

    private CacheFactory<Integer, Integer> factory;

    @Before
    public void setup() {
        factory = new DefaultCacheFactory<>();
    }

    @Test
    public void shouldProduceCorrectFactories() {
        SoftAssertions softly = new SoftAssertions();
        assertCache(softly, factory.create(CacheType.LRU, 5), 5, CacheType.LRU);
        assertCache(softly, factory.create(CacheType.MRU, 18), 18, CacheType.MRU);
        assertCache(softly, factory.create(CacheType.FIFO, 42), 42, CacheType.FIFO);
        assertCache(softly, factory.create(CacheType.RANDOM, 137), 137, CacheType.RANDOM);
        softly.assertAll();
    }

    public void assertCache(SoftAssertions assertions, Cache<Integer, Integer> cache, int capacity, CacheType type) {
        assertions.assertThat(cache.getEvictionType()).isSameAs(type);
        assertions.assertThat(cache.capacity()).isEqualTo(capacity);
    }
}
