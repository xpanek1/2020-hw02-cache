package cz.muni.fi.pb162.hw02.impl;

import cz.muni.fi.pb162.hw02.CacheType;

import java.io.IOException;
import java.util.Random;

/**
 * @author Richard Panek
 *
 * @param <K> Key type
 * @param <V> Value type
 */
public class RandomCache <K, V> extends RegularCache<K, V> {
    /**
     * Type of cache
     */
    private static final CacheType TYPE = CacheType.RANDOM;

    /**
     * Constructor
     *
     * @param maxSize MAx size of cache
     * @throws IOException if maxSize is less than 1
     */
    public RandomCache(int maxSize) throws IOException {
        super(maxSize);
    }

    /**
     * Retrieves object from cache
     *
     * @param key key under which the object is stored
     * @return object stored under given key
     */
    @Override
    public V get(K key){
        int index = this.getIndex(key);
        if (index == -1) {
            return null;
        }
        return super.get(key);
    }

    /**
     * Puts new object into cache
     *
     * @param key   key under which the object is stored
     * @param value object stored into cache
     */
    @Override
    public void put(K key, V value) {
        int index = this.getIndex(key);
        if (index != -1){
            this.refreshIndex(key, value, index);
            return;
        }
        if (this.capacity() > this.size()){
            this.add(key, value);
            return;
        }
        this.refreshIndex(key, value, new Random().nextInt(this.capacity()));
    }

    /**
     * Returns type of the cache
     *
     * @return cache type
     */
    @Override
    public CacheType getEvictionType() {
        return TYPE;
    }

}
