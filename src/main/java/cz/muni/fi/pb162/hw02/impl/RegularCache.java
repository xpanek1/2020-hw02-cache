package cz.muni.fi.pb162.hw02.impl;

import cz.muni.fi.pb162.hw02.Cache;

import java.io.IOException;
import java.util.ArrayList;

/**
 * @author Richard Panek
 *
 * @param <K> Key type
 * @param <V> Value type
 */
abstract class RegularCache<K, V> implements Cache<K, V> {

    /**
     * List of values
     */
    private final ArrayList<V> list;

    /**
     * List o keys
     */
    private final ArrayList<K> keys;

    /**
     * actual size of cache
     */
    private int size = 0;

    /**
     * Maximal size of cache
     */
    private final int maxSize;

    /**
     * Constructor of abstract class
     *
     * @param maxSize   max size of cache
     * @throws IOException  if maxSize is less than 1
     */
    RegularCache (int maxSize) throws IOException{
        if (maxSize < 1){
            throw new IOException("Invalid Cache size");
        }
        this.list = new ArrayList<>(maxSize);
        this.keys = new ArrayList<>(maxSize);
        this.maxSize = maxSize;
    }

    /**
     * Retrieves object from cache
     *
     * @param key key under which the object is stored
     * @return object stored under given key
     */
    @Override
    public V get(K key) {
        int index = this.getIndex(key);
        if (index == -1){
            return null;
        }
        return this.list.get(index);
    }

    /**
     * Returns total capacity of the cache
     *
     * @return capacity of the cache
     */
    @Override
    public int capacity() {
        return this.maxSize;
    }

    /**
     * Returns used capacity of the cache
     *
     * @return used capacity
     */
    @Override
    public int size() {
        return this.size;
    }

    /**
     * Return index of key
     *
     * @param key key of what we want to find indey
     * @return  index of key
     */
    public int getIndex(K key) {
        return this.keys.indexOf(key);
    }

    /**
     * Refreshes value on indez
     *
     * @param key   key of value
     * @param value value of index
     * @param index index we want to change
     */
    public void refreshIndex(K key, V value, int index) {
        this.keys.set(index, key);
        this.list.set(index, value);
    }

    /**
     * Appends new value to list and keys
     *
     * @param key   key we want to add
     * @param value value we want to add
     */
    public void add(K key, V value) {
        this.keys.add(key);
        this.list.add(value);
        this.size++;
    }

}
