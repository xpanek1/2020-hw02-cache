package cz.muni.fi.pb162.hw02;

/**
 * Cache interface
 * @author Jakub Cechacek
 *
 * @param <K> Key type
 * @param <V> Value type
 */
public interface Cache<K, V> {

    /**
     * Retrieves object from cache
     * @param key key under which the object is stored
     * @return object stored under given key
     */
    V get(K key);

    /**
     * Puts new object into cache
     * @param key key under which the object is stored
     * @param value object stored into cache
     */
    void put(K key, V value);

    /**
     * Returns type of the cache
     * @return cache type
     */
    CacheType getEvictionType();

    /**
     * Returns total capacity of the cache
     * @return capacity of the cache
     */
    int capacity();

    /**
     * Returns used capacity of the cache
     * @return used capacity
     */
    int size();

}
