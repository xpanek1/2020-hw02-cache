package cz.muni.fi.pb162.hw02;

/**
 * Interface for cache factories
 * @author Jakub Cechacek
 *
 * @param <K> Key type
 * @param <V> Value type
 */
public interface CacheFactory<K, V> {

    /**
     * Creates an instance of a cache
     * @param type type of the cache
     * @param size size of the cache
     * @return cache
     */
    Cache<K, V> create(CacheType type, int size);
}
