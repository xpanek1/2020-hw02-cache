package cz.muni.fi.pb162.hw02.impl;

import cz.muni.fi.pb162.hw02.CacheType;

import java.io.IOException;
import java.util.ArrayList;

/**
 * @author Richard Panek
 *
 * @param <K> Key type
 * @param <V> Value type
 */
public class LRUCache <K, V> extends RegularCache<K, V> {

    /**
     * List of usages
     */
    private final ArrayList<Integer> usage;

    /**
     * Type of cache
     */
    private static final CacheType TYPE = CacheType.LRU;

    /**
     * Constructor
     *
     * @param maxSize MAx size of cache
     * @throws IOException if maxSize is less than 1
     */
    public LRUCache(int maxSize) throws IOException {
        super(maxSize);
        this.usage = new ArrayList<>(maxSize);
    }

    /**
     * Retrieves object from cache
     *
     * @param key key under which the object is stored
     * @return object stored under given key
     */
    @Override
    public V get(K key){
        int index = this.getIndex(key);
        if (index == -1) {
            return null;
        }
        this.usage.set(index, (this.usage.get(index) + 1));
        return super.get(key);
    }

    /**
     * Puts new object into cache
     *
     * @param key   key under which the object is stored
     * @param value object stored into cache
     */
    @Override
    public void put(K key, V value) {
        int index = this.getIndex(key);
        if (index != -1){
            this.refreshIndex(key, value, index);
            this.usage.set(index, 1);
            return;
        }
        if (this.capacity() > this.size()){
            this.add(key, value);
            index = this.getIndex(key);
            this.usage.add(index, 1);
            return;
        }
        this.refreshIndex(key, value, this.min());
        index = this.getIndex(key);
        this.usage.set(index, 1);
    }

    /**
     * Returns type of the cache
     *
     * @return cache type
     */
    @Override
    public CacheType getEvictionType() {
        return TYPE;
    }

    /**
     * Returns index of LRU
     *
     * @return index of LRU
     */
    private int min() {
        int result = 0;
        for (int i = 1; i < this.capacity(); i++) {
            if (this.usage.get(result) >= 1 && this.usage.get(i) >= 1) {
                if (this.usage.get(result) > this.usage.get(i)) {
                    result = i;
                }
            }
        }
        return result;
    }
}