package cz.muni.fi.pb162.hw02.impl;

import cz.muni.fi.pb162.hw02.Cache;
import cz.muni.fi.pb162.hw02.CacheFactory;
import cz.muni.fi.pb162.hw02.CacheType;

import java.io.IOException;

/**
 * @author Richard Panek
 *
 * @param <K> Key type
 * @param <V> Value type
 */
public class DefaultCacheFactory<K, V> implements CacheFactory<K, V> {
    /**
     * Creates an instance of a cache
     *
     * @param type type of the cache
     * @param size size of the cache
     * @return cache
     */
    @Override
    public Cache<K, V> create(CacheType type, int size) {
        try {
            switch (type) {
                case LRU:
                    return new LRUCache<>(size);
                case MRU:
                    return new MRUCache<>(size);
                case FIFO:
                    return new FIFOCache<>(size);
                case RANDOM:
                    return new RandomCache<>(size);
                default:
                    return null;
            }
        } catch (IOException e) {
            System.out.println(e.toString());
            return null;
        }
    }
}
