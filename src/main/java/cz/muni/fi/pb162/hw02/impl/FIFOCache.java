package cz.muni.fi.pb162.hw02.impl;

import cz.muni.fi.pb162.hw02.CacheType;

import java.io.IOException;
import java.util.ArrayList;

/**
 * @author Richard Panek
 *
 * @param <K> Key type
 * @param <V> Value type
 */
public class FIFOCache <K, V> extends RegularCache<K, V>{
    /**
     * List for FIFO
     */
    private final ArrayList<Integer> fifo;

    /**
     * Type of cache
     */
    private static final CacheType TYPE = CacheType.FIFO;

    /**
     * FIFO cache constructor
     *
     * @param maxSize Maximal size of Cache
     * @throws IOException If maxSize is less than 1
     */
    public FIFOCache(int maxSize) throws IOException {
        super(maxSize);
        this.fifo = new ArrayList<>(maxSize);
    }

    /**
     * Retrieves object from cache
     *
     * @param key key under which the object is stored
     * @return object stored under given key
     */
    @Override
    public V get(K key){
        int index = this.getIndex(key);
        if (index == -1) {
            return null;
        }
        return super.get(key);
    }

    /**
     * Puts new object into cache
     *
     * @param key   key under which the object is stored
     * @param value object stored into cache
     */
    @Override
    public void put(K key, V value) {
        int index = this.getIndex(key);
        if (index != -1){
            this.refreshIndex(key, value, index);
            this.fifo.remove((Integer) index);
            this.fifo.add(index);
            return;
        }
        if (this.capacity() > this.size()){
            this.add(key, value);
            this.fifo.add(this.getIndex(key));
            return;
        }
        this.refreshIndex(key, value, this.fifo.get(0));
        this.fifo.remove(0);
        this.fifo.add(this.getIndex(key));
    }

    /**
     * Returns type of the cache
     *
     * @return cache type
     */
    @Override
    public CacheType getEvictionType() {
        return TYPE;
    }
}
